package com.vivy.vivydoctors.extension

import android.content.Context
import com.vivy.vivydoctors.util.ScreenUtil

/**
 * Set font size to device width
 *
 * @param fontWeight    Required size
 */
fun setFontSizeToWidth(fontWeight: Float, context: Context): Float {

    val screenWidth = ScreenUtil.width.toFloat()
    val weight: Float = ((screenWidth / 100) * fontWeight)
    val density = context.resources.displayMetrics.density - 0.5f
    return weight / density

}

/**
 * Set font size to device height
 *
 * @param fontWeight    Required size
 */
fun setFontSizeToHeight(fontWeight: Float, context: Context): Float {

    val screenHeight = ScreenUtil.height.toFloat()
    val weight: Float = ((screenHeight / 100) * fontWeight)
    val density = context.resources.displayMetrics.density - 0.5f
    return weight / density

}
