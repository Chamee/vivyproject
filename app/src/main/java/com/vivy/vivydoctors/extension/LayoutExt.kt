package com.vivy.vivydoctors.extension

import android.widget.FrameLayout
import androidx.constraintlayout.widget.ConstraintLayout
import com.vivy.vivydoctors.util.ScreenUtil

/**
 * Set vertical weight on device height
 *
 * @param weight    Weight of the screen by 100
 */
fun FrameLayout.setVerticalWeight(weight: Int) {

    val screenHeight = ScreenUtil.height
    this.layoutParams.apply {
        height = (screenHeight / 100) * weight
    }

}

/**
 * Set horizontal weight on device width
 *
 * @param weight    Weight of the screen by 100
 */
fun FrameLayout.setHorizontalWeight(weight: Int) {

    val screenWidth = ScreenUtil.width
    this.layoutParams.apply {
        width = (screenWidth / 100) * weight
    }

}

/**
 * Set size on device height
 *
 * @param weight    Weight of the screen by 100
 */
fun FrameLayout.setSizeOnVerticalWeight(weight: Int) {

    val screenHeight = ScreenUtil.height
    this.layoutParams.apply {
        height = (screenHeight / 100) * weight
        width = (screenHeight / 100) * weight
    }

}

/**
 * Set size on device width
 *
 * @param weight    Weight of the screen by 100
 */
fun FrameLayout.setSizeOnHorizontalWeight(weight: Int) {

    val screenWidth = ScreenUtil.width
    this.layoutParams.apply {
        height = (screenWidth / 100) * weight
        width = (screenWidth / 100) * weight
    }

}

/**
 * Set vertical weight on device height
 *
 * @param weight    Weight of the screen by 100
 */
fun ConstraintLayout.setVerticalWeight(weight: Int) {

    val screenHeight = ScreenUtil.height
    this.layoutParams.apply {
        height = (screenHeight / 100) * weight
    }

}