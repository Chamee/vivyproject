package com.vivy.vivydoctors.feature.vivydoctorlist

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.base.app.config.AppConfig
import com.vivy.vivydoctors.BR
import com.vivy.vivydoctors.R
import com.vivy.vivydoctors.base.BaseFragment
import com.vivy.vivydoctors.databinding.FragmentDoctorListBinding
import com.vivy.vivydoctors.feature.ViewModelProviderFactory
import com.vivy.vivydoctors.feature.activity.dashboard.DashboardActivity
import com.vivy.vivydoctors.feature.doctordetails.DoctorDetailsFragment
import com.vivy.vivydoctors.feature.vivydoctorlist.adapter.DoctorListRecyclerViewAdapter
import com.vivy.vivydoctors.model.response.DoctorDetails
import com.vivy.vivydoctors.util.CommonUtils
import kotlinx.android.synthetic.main.fragment_doctor_list.*
import javax.inject.Inject

class DoctorListFragment : BaseFragment<FragmentDoctorListBinding, DoctorListFragmentViewModel>(), DoctorListRecyclerViewAdapter.DoctorListAdapterListener {

    private var mActivity: DashboardActivity? = null
    private var fragmentDoctorListBinding: FragmentDoctorListBinding? = null

    @Inject
    lateinit var factory: ViewModelProviderFactory
    @Inject
    lateinit var doctorListRecyclerViewAdapter: DoctorListRecyclerViewAdapter
    @Inject
    lateinit var searchListRecyclerViewAdapter: DoctorListRecyclerViewAdapter

    private lateinit var doctorListViewModel: DoctorListFragmentViewModel
    private lateinit var doctorsListRecyclerView: RecyclerView
    private lateinit var searchListRecyclerView: RecyclerView
    var tempArrayList : ArrayList<DoctorDetails> = arrayListOf()
    var filteredList: ArrayList<DoctorDetails> = arrayListOf()

    private var loading = false
    private var lastVisibleItemPosition: Int = 0
    private var visibleItemCount: Int = 0
    private var totalItemCount: Int = 0

    override val bindingVariable: Int
        get() = BR.viewModel
    override val layoutId: Int
        get() = R.layout.fragment_doctor_list
    override val viewModel: DoctorListFragmentViewModel
        get() {
            doctorListViewModel = ViewModelProviders.of(
                this,
                factory
            ).get(DoctorListFragmentViewModel::class.java)
            return doctorListViewModel
        }

    companion object{
        fun newInstance() = DoctorListFragment()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        super.onViewCreated(view, savedInstanceState)

        fragmentDoctorListBinding = getViewDataBinding()

        initialize()

        defineLayout()

        getData()

        setData()

        setListeners()
    }

    /**
     * initialize activity, adapters and global lists
     */
    private fun initialize() {

        doctorListRv?.apply {
            adapter = doctorListRecyclerViewAdapter
            layoutManager = LinearLayoutManager(context)
        }

        searchListRv?.apply {
            adapter = searchListRecyclerViewAdapter
            layoutManager = LinearLayoutManager(context)
        }

    }

    /**
     * define the components in layout
     */
    private fun defineLayout() {

        doctorsListRecyclerView = doctorListRv
        doctorsListRecyclerView.layoutManager =
            LinearLayoutManager(this.context, LinearLayoutManager.VERTICAL, false)
        doctorListRecyclerViewAdapter.notifyDataSetChanged()

        searchListRecyclerView = searchListRv
        searchListRecyclerView.layoutManager =
            LinearLayoutManager(this.context, LinearLayoutManager.VERTICAL, false)
        searchListRecyclerViewAdapter.notifyDataSetChanged()

    }

    /**
     * Get Data from APIs and other resources
     */
    private fun getData() {

        viewModel.getDoctorListData().observe(this, Observer {
            getInitialData(it.doctors)
        })

    }

    /**
     * Set data to the UI components
     */
    private fun setData() {

    }

    /**
     * Set listeners
     */
    private fun setListeners() {

        doctorListRv.addOnScrollListener(onScrollListener)
        doctorListRecyclerViewAdapter.onItemClick(this)
        searchListRecyclerViewAdapter.onItemClick(this)

        searchBar.addTextChangedListener(textChangeListener())

        searchCloseBtn.setOnClickListener {onCloseButtonClick()}

        searchBtn.setOnClickListener { searchDoctor(searchBar.editableText) }

    }

    /**
     * First data set from API
     * @param doctors: data set
     */
    private fun getInitialData(doctors: ArrayList<DoctorDetails>) {
        recentAccountSorting()
        viewModel.doctorDataList?.addAll(doctors)
        doctorListRecyclerViewAdapter.addItems(viewModel.doctorDataList!!)

    }

    /**
     * UI handler to get last element and load more items
     */
    private val onScrollListener = object : RecyclerView.OnScrollListener() {
        override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
            super.onScrollStateChanged(recyclerView, newState)
        }

        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            super.onScrolled(recyclerView, dx, dy)
            visibleItemCount = doctorListRv.layoutManager!!.getChildCount()
            totalItemCount = doctorListRv.layoutManager!!.getItemCount()
            var layOut = doctorListRv.layoutManager as LinearLayoutManager
            lastVisibleItemPosition = layOut.findLastVisibleItemPosition()
            if (!loading && lastVisibleItemPosition == totalItemCount - 1 && totalItemCount > 0) {

                if(viewModel.lastPageKey != ""){
                    Toast.makeText(context, "Load More Data", Toast.LENGTH_SHORT).show()
                    loading = true
                    loadMoreItems()
                }else{
                    loading = true
                    Toast.makeText(context, "End of the List", Toast.LENGTH_SHORT).show()
                }

            }
        }
    }

    /**
     * Get data from next pages
     */
    private fun loadMoreItems() {

        viewModel.getMoreDataSet(
            id = viewModel.lastPageKey,
            onSuccess = {
                loading = false
                viewModel.doctorDataList?.addAll(it)
                recentAccountSorting()
                doctorListRecyclerViewAdapter.addItems(viewModel.doctorDataList!!)
                doctorListRecyclerViewAdapter.notifyDataSetChanged()
            },
            onFail = {}

        )
    }

    override fun onElementClick(doctorDetails: DoctorDetails) {
        addItemsToSearchList(doctorDetails)
        recentAccountSorting()
        val bundle = Bundle()
        bundle.putParcelable(CommonUtils.DOCTOR_DETAILS, doctorDetails)
        addFragment(DoctorDetailsFragment(), CommonUtils.DOCTOR_DETAILS, bundle)
    }

    /**
     * Add selected item to recent search list
     * If item already available at search list prioritized the element
     *
     * Searched elements remove from main list
     * if search count is more than search limit remove most recent elements from searchList
     * and add to the main list
     *
     * @param: doctorDetails: Selected Item
     */
    private fun addItemsToSearchList(doctorDetails: DoctorDetails) {

        tempArrayList.remove(doctorDetails)
        tempArrayList.add(0,doctorDetails)
        for (item in tempArrayList){
            viewModel.doctorDataList?.remove(item)
        }

        if(!tempArrayList.contains(doctorDetails)){
            if(tempArrayList.size > AppConfig.RECENT_SEARCH_COUNT){
                for (a in AppConfig.RECENT_SEARCH_COUNT until tempArrayList.size){
                    viewModel.doctorDataList?.add(tempArrayList[a-1])
                    tempArrayList.remove(tempArrayList[a-1])
                }
            }
        }

    }

    /**
     * Sort main List according to the doctor rating without considering searched items
     * remove duplicate of elements
     * add search items on the top of the list
     */
    private fun recentAccountSorting() {

        viewModel.doctorDataList?.sortByDescending { it.rating }

        for(a in tempArrayList.size-1 downTo 0){
            doctorListViewModel.doctorDataList?.add(0,tempArrayList.get(a))
        }
        if (!viewModel.doctorDataList.isNullOrEmpty() ){
            viewModel.doctorDataList = viewModel.doctorDataList?.distinctBy { it.id } as ArrayList<DoctorDetails>?
        }

        doctorListRecyclerViewAdapter.recentSearchLength = tempArrayList.size
        doctorListRecyclerViewAdapter.addItems(viewModel.doctorDataList!!)
    }

    /**
     * Search bar Listener
     */
    private fun textChangeListener(): TextWatcher? {
        return object : TextWatcher {

            override fun afterTextChanged(s: Editable) {
                searchDoctor(s)

            }

            override fun beforeTextChanged(s: CharSequence, start: Int,
                                           count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence, start: Int,
                                       before: Int, count: Int) {
            }
        }
    }

    /**
     * Search doctor by name
     * @param text : input Char Sequence
     */
    private fun searchDoctor(text: Editable) {
        when {
            text.toString().length > 2 -> {
                var tempList: ArrayList<DoctorDetails> = arrayListOf()
                for(item in filteredList){
                    if(item.name.contains(text.toString(),true))
                        tempList.add(item)
                }
                filteredList.clear()
                filteredList.addAll(tempList)

                if (!filteredList.isNullOrEmpty() ){
                    filteredList = (filteredList.distinctBy { it.id } as ArrayList<DoctorDetails>?)!!
                }

                doctorsListRecyclerView.visibility = View.GONE
                searchListRecyclerView.visibility = View.VISIBLE
                searchListRecyclerViewAdapter.addItems(filteredList)
                searchListRecyclerViewAdapter.isSearchView = true
                searchListRecyclerViewAdapter.notifyDataSetChanged()
            }
            text.toString().length == 2 -> {
                viewModel.doctorDataList?.let { filteredList.addAll(it) }
            }
            else -> {
                filteredList.clear()
                doctorsListRecyclerView.visibility = View.VISIBLE
                searchListRecyclerView.visibility = View.GONE
                doctorListRecyclerViewAdapter.notifyDataSetChanged()
            }
        }
    }

    /**
     * Click search close button
     */
    private fun onCloseButtonClick() {
        searchBar.setText("")
        filteredList.clear()
        doctorsListRecyclerView.visibility = View.VISIBLE
        searchListRecyclerView.visibility = View.GONE
        doctorListRecyclerViewAdapter.notifyDataSetChanged()
    }

}
