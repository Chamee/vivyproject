package com.vivy.vivydoctors.feature.vivydoctorlist

import com.vivy.vivydoctors.feature.activity.dashboard.DashboardActivity
import com.vivy.vivydoctors.feature.vivydoctorlist.adapter.DoctorListRecyclerViewAdapter
import dagger.Module
import dagger.Provides

@Module
class DoctorListFragmentModule {

    @Provides
    internal fun providerDoctorAdapter(activity: DashboardActivity) : DoctorListRecyclerViewAdapter{
        return DoctorListRecyclerViewAdapter(
            ArrayList(),
            activity
        )
    }
}