package com.vivy.vivydoctors.feature.vivydoctorlist.adapter

import com.vivy.vivydoctors.model.response.DoctorDetails

/**
 * Card View Details
 */
class DoctorListRecyclerViewModel(
    private val doctorDetails : DoctorDetails,
    val doctorListViewModelListener: DoctorListViewModelListener
) {

    fun onItemClick(){
        doctorListViewModelListener.onItemClick(doctorDetails)
    }

    interface DoctorListViewModelListener{
        fun onItemClick(doctorDetails: DoctorDetails)
    }
}

/**
 * Empty Card View Details
 */
class DoctorListRecyclerEmptyViewModel(){
}