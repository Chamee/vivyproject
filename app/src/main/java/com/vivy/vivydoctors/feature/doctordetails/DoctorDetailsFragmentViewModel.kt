package com.vivy.vivydoctors.feature.doctordetails

import android.content.Context
import androidx.lifecycle.MutableLiveData
import com.vivy.vivydoctors.base.BaseViewModel
import com.vivy.vivydoctors.manager.DataManager
import com.vivy.vivydoctors.model.response.DoctorDetails

class DoctorDetailsFragmentViewModel(dataManager: DataManager, context: Context) :
    BaseViewModel(dataManager, context){

    val doctorDetails: MutableLiveData<DoctorDetails> by lazy {
        MutableLiveData<DoctorDetails>()
    }
}