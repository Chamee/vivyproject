package com.vivy.vivydoctors.feature.vivydoctorlist.adapter

import android.opengl.Visibility
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.base.app.config.AppConfig
import com.vivy.vivydoctors.R
import com.vivy.vivydoctors.base.BaseViewHolder
import com.vivy.vivydoctors.databinding.ComponentDoctorListitemBinding
import com.vivy.vivydoctors.databinding.ComponentDoctorListitemEmptyBinding
import com.vivy.vivydoctors.feature.activity.dashboard.DashboardActivity
import com.vivy.vivydoctors.model.response.DoctorDetails
import kotlin.coroutines.coroutineContext

class DoctorListRecyclerViewAdapter(
    private val list: ArrayList<DoctorDetails>?,
    private val hostActivity: DashboardActivity?
) : RecyclerView.Adapter<BaseViewHolder>() {

    var mListener: DoctorListAdapterListener? = null
    var adapterType: String? = null
    var recentSelection : ArrayList<DoctorDetails>? = arrayListOf()
    var recentSearchLength: Int = 0
    var isSearchView: Boolean = false


    /**
     * Set View Layout and return the view binding
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        when(viewType){
            VIEW_TYPE_DOCTORLIST -> {
                val doctorDetailsBainding = ComponentDoctorListitemBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,false
                )

                return DoctorDetailsViewHolder(doctorDetailsBainding)
            }
            EMPTY_LIST -> {
                val doctorDetailsEmptyBinding = ComponentDoctorListitemEmptyBinding.inflate(
                    LayoutInflater.from((parent.context)),
                    parent, false
                )

                return DoctorDetailsEmptyViewHolder(doctorDetailsEmptyBinding)
            }

            else -> {
                val doctorDetailsEmptyBinding = ComponentDoctorListitemEmptyBinding.inflate(
                    LayoutInflater.from((parent.context)),
                    parent, false
                )

                return DoctorDetailsEmptyViewHolder(doctorDetailsEmptyBinding)
            }

        }
    }

    /**
     * return the item list item count
     */
    override fun getItemCount(): Int {
        return if (list != null && list.size > 0) {
            list.size
        } else {
            1
        }
    }

    /**
     * return the view type
     */
    override fun getItemViewType(position: Int): Int {
        return if (list != null && list.isNotEmpty()) {
            VIEW_TYPE_DOCTORLIST
        } else {
            EMPTY_LIST
        }
    }

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        holder.setIsRecyclable(false)
        holder.onBind(position)
    }

    /**
     * load items to the item list
     */
    fun addItems(accounts: ArrayList<DoctorDetails>) {
        clearItems()
        list!!.addAll(accounts)
        notifyDataSetChanged()
    }


    /**
     * clear the item list
     */
    fun clearItems() {
        list!!.clear()
    }


    /**
     * Init the callback listener
     */
    fun onItemClick(listener: DoctorListAdapterListener) {
        this.mListener = listener
    }

    interface DoctorListAdapterListener{
        fun onElementClick(doctorDetails: DoctorDetails)
    }

    /**
     * Doctor details list
     */
    inner class DoctorDetailsViewHolder(val mBinding: ComponentDoctorListitemBinding):
        BaseViewHolder(mBinding.root),
        DoctorListRecyclerViewModel.DoctorListViewModelListener{

        private var doctorListRecyclerViewModel: DoctorListRecyclerViewModel? = null

        override fun onBind(position: Int) {
            val item = list!![position]

            doctorListRecyclerViewModel =
                DoctorListRecyclerViewModel(
                    item,this
                )

            if (!isSearchView) {
                if (recentSearchLength > 0 && position == 0) {
                    mBinding.titleWrap.visibility = View.VISIBLE
                    mBinding.title.setTextViewText("Recent Doctors")
                }

                if (recentSearchLength == position && recentSearchLength < AppConfig.RECENT_SEARCH_COUNT + 1) {
                    mBinding.titleWrap.visibility = View.VISIBLE
                    mBinding.title.setTextViewText("Vivy Doctors")
                } else if (recentSearchLength >= AppConfig.RECENT_SEARCH_COUNT + 1 && position == AppConfig.RECENT_SEARCH_COUNT) {
                    mBinding.titleWrap.visibility = View.VISIBLE
                    mBinding.title.setTextViewText("Vivy Doctors")
                }
            }

            mBinding.viewModel = doctorListRecyclerViewModel
            mBinding.name.setTextViewText(item.name)
            mBinding.address.setTextViewText(item.address)
            if(item.photoId != null){
                mBinding.image.setImageUri(item.photoId)
            }else{
                mBinding.image.setImage(ContextCompat.getDrawable(hostActivity!!.applicationContext,R.drawable.ic_avatar))
            }

        }

        override fun onItemClick(doctorDetails: DoctorDetails) {
            mListener?.onElementClick(doctorDetails)
        }

    }

    /**
     * Empty list of doctor
     */
    inner class DoctorDetailsEmptyViewHolder(val mBinding: ComponentDoctorListitemEmptyBinding) :
        BaseViewHolder(mBinding.root){
        override fun onBind(position: Int) {

        }
    }

    companion object{
        const val VIEW_TYPE_DOCTORLIST = 0
        const val EMPTY_LIST = 1
    }
}