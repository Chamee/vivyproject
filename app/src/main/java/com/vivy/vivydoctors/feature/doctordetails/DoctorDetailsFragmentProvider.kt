package com.vivy.vivydoctors.feature.doctordetails

import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class DoctorDetailsFragmentProvider {

    @ContributesAndroidInjector
    abstract fun providerDoctorDetailsFragmentFactory() : DoctorDetailsFragment
}