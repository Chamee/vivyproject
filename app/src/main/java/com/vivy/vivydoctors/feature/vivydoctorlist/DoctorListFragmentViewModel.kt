package com.vivy.vivydoctors.feature.vivydoctorlist

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.vivy.vivydoctors.api.payload.DoctorListApi
import com.vivy.vivydoctors.base.BaseViewModel
import com.vivy.vivydoctors.feature.vivydoctorlist.adapter.DoctorListRecyclerViewAdapter
import com.vivy.vivydoctors.manager.DataManager
import com.vivy.vivydoctors.model.response.DoctorDetails
import com.vivy.vivydoctors.model.response.DoctorsResponse

class DoctorListFragmentViewModel(dataManager: DataManager, context: Context) :
    BaseViewModel(dataManager, context) {

    var doctorDataList: ArrayList<DoctorDetails>? = arrayListOf()
    var adapter = DoctorListRecyclerViewAdapter
    var lastPageKey: String = ""


    val initialDoctorList: MutableLiveData<DoctorsResponse> by lazy {
        MutableLiveData<DoctorsResponse>().also {
            getFirstDataSet()
        }
    }

//    val moreDoctorList: MutableLiveData<DoctorsResponse> by lazy {
//        MutableLiveData<DoctorsResponse>().also {
//            getMoreDataSet(lastPageKey)
//        }
//    }

    fun getDoctorListData(): LiveData<DoctorsResponse> {
        return initialDoctorList
    }

    fun getFirstDataSet() {
        setProgressLoadingText("Loading")
        DoctorListApi().getDoctorDetails().map {
            initialDoctorList.value = it
            lastPageKey = it.lastKey
        }.onErrorReturn {
            onError(it)
        }.subscribe()
    }


//    fun moreDoctorList(): LiveData<DoctorsResponse> {
//        return moreDoctorList
//    }

    fun getMoreDataSet(
        id: String,
        onSuccess: (doctorDetails : ArrayList<DoctorDetails>) -> Unit,
        onFail: () -> Unit
    ) {
        DoctorListApi().getMoreDetails(id).map {
//            doctorDataList?.addAll(it.doctors)
            if (it.lastKey != null){
                lastPageKey = it.lastKey
            }else{
                lastPageKey = ""
            }
            onSuccess.invoke(it.doctors)
        }.onErrorReturn {
            onError(it)
            onFail.invoke()
        }.subscribe()


    }

}