package com.vivy.vivydoctors.feature

import android.app.Activity
import android.app.Application
import com.vivy.vivydoctors.di.component.DaggerApplicationComponent
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import javax.inject.Inject

class BaseApplication : Application(), HasActivityInjector {

    @Inject
    internal lateinit var activityDispatchingAndroidInjector: DispatchingAndroidInjector<Activity>

    override fun activityInjector() = activityDispatchingAndroidInjector

    override fun onCreate() {
        super.onCreate()
        mInstance = this
        DaggerApplicationComponent.builder()
            .application(this)
            .build()
            .inject(this)
    }

    companion object{
        private var mInstance: BaseApplication? = null

        fun getInstance(): BaseApplication?{
            return mInstance
        }
    }

}