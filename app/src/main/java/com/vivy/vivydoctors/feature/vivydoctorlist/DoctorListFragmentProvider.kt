package com.vivy.vivydoctors.feature.vivydoctorlist

import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class DoctorListFragmentProvider {

    @ContributesAndroidInjector(modules = [DoctorListFragmentModule::class])
    abstract fun provideDoctorListFragmentFactory() : DoctorListFragment
}