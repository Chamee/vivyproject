package com.vivy.vivydoctors.feature.activity

import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.os.Handler
import android.util.DisplayMetrics
import androidx.appcompat.app.AppCompatActivity
import com.vivy.vivydoctors.R
import com.vivy.vivydoctors.feature.activity.dashboard.DashboardActivity
import com.vivy.vivydoctors.util.ScreenUtil

class SplashActivity : AppCompatActivity() {

    private val SplashDelay: Long = 3000
    private lateinit var mDelayHandler: Handler
    private lateinit var mRunnable: Runnable

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        val displayMetrics = DisplayMetrics()
        windowManager.defaultDisplay.getMetrics(displayMetrics)

        ScreenUtil.height = displayMetrics.heightPixels
        ScreenUtil.width = displayMetrics.widthPixels


        //Initialize the Handler
        mDelayHandler = Handler()
        mRunnable = Runnable {
            run {
                startActivity(Intent(applicationContext, DashboardActivity::class.java))
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                this.finish()
                finish()
            }
        }

        //Navigate with delay
        mDelayHandler.postDelayed(mRunnable, SplashDelay)

    }

    override fun onDestroy() {
        super.onDestroy()
        mDelayHandler.removeCallbacks(mRunnable)
    }
}