package com.vivy.vivydoctors.feature.doctordetails

import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProviders
import com.vivy.vivydoctors.BR
import com.vivy.vivydoctors.R
import com.vivy.vivydoctors.base.BaseFragment
import com.vivy.vivydoctors.databinding.FragmentDoctorDetailsBinding
import com.vivy.vivydoctors.feature.ViewModelProviderFactory
import com.vivy.vivydoctors.feature.activity.dashboard.DashboardActivity
import com.vivy.vivydoctors.model.response.DoctorDetails
import com.vivy.vivydoctors.util.CommonUtils
import kotlinx.android.synthetic.main.fragment_doctor_details.address
import kotlinx.android.synthetic.main.fragment_doctor_details.image
import kotlinx.android.synthetic.main.fragment_doctor_details.name
import javax.inject.Inject

class DoctorDetailsFragment : BaseFragment<FragmentDoctorDetailsBinding, DoctorDetailsFragmentViewModel>() {

    private var mActivity: DashboardActivity? = null
    private var fragmentDoctorDetailBinding: FragmentDoctorDetailsBinding? = null

    @Inject
    lateinit var factory: ViewModelProviderFactory

    private lateinit var doctorDetailsFragmentViewModel: DoctorDetailsFragmentViewModel

    override val bindingVariable: Int
        get() = BR.viewModel
    override val layoutId: Int
        get() = R.layout.fragment_doctor_details
    override val viewModel: DoctorDetailsFragmentViewModel
        get() {
            doctorDetailsFragmentViewModel = ViewModelProviders.of(
                this,
                factory
            ).get(DoctorDetailsFragmentViewModel::class.java)
            return doctorDetailsFragmentViewModel
        }

    companion object{
        fun newInstance() = DoctorDetailsFragment()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        fragmentDoctorDetailBinding = getViewDataBinding()

        initialize()

        defineLayout()

        getData()

        setData()

        setListeners()
    }

    /**
     * initialize activity, adapters and global lists
     */
    private fun initialize() {
        mActivity = activity as? DashboardActivity
    }

    /**
     * define the components in layout
     */
    private fun defineLayout() {}

    /**
     * Get Data from APIs and other resources
     */
    private fun getData() {
        viewModel.doctorDetails.value = arguments?.getParcelable<DoctorDetails>(CommonUtils.DOCTOR_DETAILS)
    }

    /**
     * Set data to the UI components
     */
    private fun setData() {

        viewModel.doctorDetails.value?.name?.let { name.setTextViewText(it) }
        viewModel.doctorDetails.value?.address?.let { address.setTextViewText(it) }

        if(viewModel.doctorDetails.value?.photoId != null){
            image.setImageUri(viewModel.doctorDetails.value?.photoId!!)
        }else{
            image.setImage(ContextCompat.getDrawable(mActivity!!.applicationContext,R.drawable.ic_avatar))
        }

    }

    /**
     * Set listeners
     */
    private fun setListeners() {}

}