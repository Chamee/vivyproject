package com.vivy.vivydoctors.feature

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.vivy.vivydoctors.feature.activity.dashboard.DashboardActivityViewModel
import com.vivy.vivydoctors.feature.doctordetails.DoctorDetailsFragmentViewModel
import com.vivy.vivydoctors.feature.vivydoctorlist.DoctorListFragmentViewModel
import com.vivy.vivydoctors.manager.DataManager
import java.lang.IllegalArgumentException
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ViewModelProviderFactory @Inject
constructor(
    val dataManager: DataManager,
    val context: Context
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return when{

            // Dashboard Activity View Model
            modelClass.isAssignableFrom(DashboardActivityViewModel::class.java) -> DashboardActivityViewModel(
                dataManager,
                context
            )as T

            modelClass.isAssignableFrom(DoctorListFragmentViewModel::class.java) -> DoctorListFragmentViewModel(
                dataManager,
                context
            )as T

            modelClass.isAssignableFrom(DoctorDetailsFragmentViewModel::class.java) -> DoctorDetailsFragmentViewModel(
                dataManager,
                context
            )as T

            else -> throw IllegalArgumentException("Unknown ViewModel class: " + modelClass.name)
        }
    }
}