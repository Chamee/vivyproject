package com.vivy.vivydoctors.feature.activity.dashboard

import android.app.Activity
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.vivy.vivydoctors.BR
import com.vivy.vivydoctors.R
import com.vivy.vivydoctors.base.BaseActivity
import com.vivy.vivydoctors.databinding.ActivityDashboardBinding
import com.vivy.vivydoctors.feature.ViewModelProviderFactory
import com.vivy.vivydoctors.feature.vivydoctorlist.DoctorListFragment
import com.vivy.vivydoctors.manager.NavigationController
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import javax.inject.Inject

class DashboardActivity : BaseActivity<ActivityDashboardBinding, DashboardActivityViewModel>(),
    HasSupportFragmentInjector{

    private lateinit var mActivity: Activity

    @Inject
    lateinit var factory: ViewModelProviderFactory
    private lateinit var dashboardViewModel: DashboardActivityViewModel
    private var activityDashboardBinding: ActivityDashboardBinding? = null

    @Inject
    lateinit var fragmentDispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>

    override val bindingVariable: Int
        get() = BR.viewModel
    override val layoutId: Int
        get() = R.layout.activity_dashboard
    override val viewModel: DashboardActivityViewModel
        get() {
            dashboardViewModel =
                ViewModelProviders.of(this, factory).get(DashboardActivityViewModel::class.java)
            return dashboardViewModel
        }

    override fun supportFragmentInjector(): AndroidInjector<Fragment> {

        return fragmentDispatchingAndroidInjector

    }

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        activityDashboardBinding = getViewDataBinding()

        initialize()

        getData()

        setData()

        setListeners()

    }

    /**
     * initialize
     */
    private fun initialize() {

        mActivity = this

    }

    /**
     * get data
     */
    private fun getData() {}

    /**
     * set data
     */
    private fun setData() {

        DoctorListFragment.newInstance().arguments = intent.extras
        val transaction = supportFragmentManager.beginTransaction()
        transaction.add(R.id.container, DoctorListFragment.newInstance())
        transaction.commit()

    }

    /**
     * set listeners
     */
    private fun setListeners() {}


}