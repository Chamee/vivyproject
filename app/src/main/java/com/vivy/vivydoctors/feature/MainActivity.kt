package com.vivy.vivydoctors.feature

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.vivy.vivydoctors.R

class MainActivity: AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}