package com.vivy.vivydoctors.feature.activity.dashboard

import android.content.Context
import com.vivy.vivydoctors.manager.DataManager
import dagger.Module
import dagger.Provides

@Module
class DashboardActivityModule {
    @Provides
    fun provideDashboardViewModel(dataManager: DataManager, context: Context): DashboardActivityViewModel{
        return DashboardActivityViewModel(dataManager, context)
    }
}