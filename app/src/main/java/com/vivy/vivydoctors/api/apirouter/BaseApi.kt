package com.vivy.vivydoctors.api.apirouter

import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

open class BaseApi {

    fun <T> getData(observer: Observable<T>): Observable<T> {
        return observer.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }
}