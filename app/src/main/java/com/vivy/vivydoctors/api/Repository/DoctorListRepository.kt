package com.vivy.vivydoctors.api.Repository

import com.vivy.vivydoctors.model.response.DoctorDetails
import com.vivy.vivydoctors.model.response.DoctorsResponse
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Path

interface DoctorListRepository {

    @GET("doctors.json")
    fun getFirstDataSet(): Observable<DoctorsResponse>

    @GET("doctors-{id}.json")
    fun getMoreDataSet(@Path("id") doctorId: String): Observable<DoctorsResponse>
}