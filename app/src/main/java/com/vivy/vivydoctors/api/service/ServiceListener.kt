package com.base.app.api.service

interface ServiceListener{
    fun onServiceStart(isForeground : Boolean)
    fun onServiceStop(isForeground : Boolean)
}