package com.vivy.vivydoctors.api.payload

import com.vivy.vivydoctors.api.Repository.DoctorListRepository
import com.vivy.vivydoctors.api.apirouter.ApiClient
import com.vivy.vivydoctors.api.apirouter.BaseApi
import com.vivy.vivydoctors.model.response.DoctorsResponse
import io.reactivex.Observable


class DoctorListApi : BaseApi() {

    private val doctorListRepo : DoctorListRepository by lazy {
        ApiClient.retrofit.create(DoctorListRepository::class.java)
    }

    /**
     * Get initial doctors details
     */
    fun getDoctorDetails() : Observable<DoctorsResponse> {
        return getData(doctorListRepo.getFirstDataSet())
    }

    /**
     * Get more doctor details by nextPage ID
     */
    fun getMoreDetails(id : String) : Observable<DoctorsResponse> {
        return getData(doctorListRepo.getMoreDataSet(id))
    }
}