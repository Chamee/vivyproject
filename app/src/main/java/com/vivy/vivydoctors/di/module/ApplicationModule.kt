package com.vivy.vivydoctors.di.module

import android.app.Application
import android.content.Context
import com.vivy.vivydoctors.manager.DataManager
import com.vivy.vivydoctors.manager.PreferencesManager
import com.vivy.vivydoctors.manager.impl.AppDataManager
import com.vivy.vivydoctors.manager.impl.AppPreferencesManager
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ApplicationModule {

    @Provides
    @Singleton
    fun provideContext(application: Application): Context {
        return application
    }

    @Provides
    @Singleton
    fun providePreferencesHelper(appPreferencesHelper: AppPreferencesManager): PreferencesManager {
        return appPreferencesHelper
    }

    @Provides
    @Singleton
    internal fun provideDataManager(appDataManager: AppDataManager): DataManager {
        return appDataManager
    }
}