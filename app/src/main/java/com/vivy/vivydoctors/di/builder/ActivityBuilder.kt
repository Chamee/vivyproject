package com.vivy.vivydoctors.di.builder

import com.vivy.vivydoctors.feature.activity.dashboard.DashboardActivity
import com.vivy.vivydoctors.feature.activity.dashboard.DashboardActivityModule
import com.vivy.vivydoctors.feature.doctordetails.DoctorDetailsFragmentProvider
import com.vivy.vivydoctors.feature.vivydoctorlist.DoctorListFragmentProvider
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilder {
    @ContributesAndroidInjector(
        modules = [
            DashboardActivityModule::class,
            DoctorListFragmentProvider::class,
            DoctorDetailsFragmentProvider::class
        ]

    )
    abstract fun bindDashboardActivity(): DashboardActivity
}