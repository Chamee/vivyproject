package com.vivy.vivydoctors.di.component

import android.app.Application
import com.vivy.vivydoctors.di.builder.ActivityBuilder
import com.vivy.vivydoctors.di.module.ApplicationModule
import com.vivy.vivydoctors.feature.BaseApplication
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [(AndroidInjectionModule::class),(ApplicationModule::class),(ActivityBuilder::class)])
interface ApplicationComponent {

    @Component.Builder
    interface Builder{
        @BindsInstance
        fun application(application: Application) : Builder

        fun build(): ApplicationComponent
    }

    fun inject(app: BaseApplication)
}