package com.vivy.vivydoctors.manager

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.Lifecycle
import com.vivy.vivydoctors.R
import com.vivy.vivydoctors.feature.activity.dashboard.DashboardActivity

object NavigationController {

    var fragmentInAction: Fragment? = null
    val actionStack: MutableList<String> = mutableListOf()

    private fun isEmpty() = actionStack.isEmpty()

    private fun size() = actionStack.size

    private fun pushStack(item: String) = actionStack.add(item)

    private fun popStack(): String? {
        val item = actionStack.lastOrNull()
        if (!isEmpty()) {
            actionStack.removeAt(actionStack.size - 1)
        }
        return item
    }

    private fun peek(): String? = actionStack.lastOrNull()

    fun pushFragment(
        fragmentManager: FragmentManager,
        fragment: Fragment,
        tagName: String,
        bundle: Bundle? = null
    ) {
        bundle?.let {
            fragment.arguments = it
        }
        val ft = fragmentManager.beginTransaction()

        val prevFragment = fragmentManager.findFragmentByTag(tagName)
        if (prevFragment != null) {
            ft.remove(prevFragment)
        }

        ft.add(R.id.container, fragment, tagName)
        ft.addToBackStack(tagName)
        tagName.let { it1 -> pushStack(it1) }
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
        ft.commit()

    }

    fun popFragment() {

    }

    /**
     * open another Activity
     * @param activity : Current Activity
     * @param toActivity : To Activity
     * @param isCloseLastActivity : need to close last Activity
     */
    fun openActivity(
        activity: Activity,
        toActivity: Class<*>,
        isCloseLastActivity: Boolean = false
    ) {
        val intent = Intent(activity, toActivity)
        if (isCloseLastActivity) {
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            activity.finish()
        }
        activity.startActivity(intent)
    }


    /**
     * open another Activity
     * @param activity : Current Activity
     * @param fragment : To Fragment
     * @param bundleData : Data to Pass Fragment
     */
    fun openActionActivity(
        activity: Activity?,
        fragment: Fragment,
        bundleData: Bundle? = null
    ) {
        fragmentInAction = fragment
        bundleData?.let {
            fragmentInAction?.arguments = it
        }
        val intent = Intent(activity, DashboardActivity::class.java)
        activity?.startActivity(intent)
    }

    /**
     * Add Fragment to Action Activity
     * @param bundleData : Data to Pass
     * @param fragment : fragment to Add,
     * @param tagName : String to Tag fragment
     * @param activity : AppCompatActivity
     */
    fun pushFragmentToAction(
        activity: AppCompatActivity,
        fragment: Fragment,
        tagName: String,
        bundleData: Bundle? = null
    ) {
        activity.let {
            if (it.lifecycle.currentState.isAtLeast(Lifecycle.State.RESUMED)) {

                bundleData?.let {
                    fragment.arguments = it
                }
                val fragmentManager = activity.supportFragmentManager
                val ft = fragmentManager.beginTransaction()

                val prevFragment = fragmentManager.findFragmentByTag(tagName)
                if (prevFragment != null) {
                    ft.remove(prevFragment)
                }

                ft.add(R.id.container, fragment, tagName)
                ft.addToBackStack(tagName)
                tagName.let { it1 -> pushStack(it1) }
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                ft.commit()

            }
        }
    }

    /**
     * remove Fragment from Action Activity
     * @param activity : ACtion Activity
     * @param numberOfTimes : number of Fragment to remove
     */
    fun popFragmentFromAction(activity: AppCompatActivity, numberOfTimes: Int = 1) {
        try {
            val fragmentManager = activity.supportFragmentManager
            for (item in 1..numberOfTimes) {
                popStack()?.let {
                    fragmentManager.popBackStack(it, FragmentManager.POP_BACK_STACK_INCLUSIVE)
                } ?: run {
                    activity.finish()
                }
            }
        } catch (ignored: IllegalStateException) {
            // There's no way to avoid getting this if saveInstanceState has already been called.
        }
    }
}