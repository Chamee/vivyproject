package com.vivy.vivydoctors.manager

interface PreferencesManager {

    var vivyDoctorName: String?

    var vivyDoctorId: String?
}