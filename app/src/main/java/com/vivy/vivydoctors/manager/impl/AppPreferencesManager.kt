package com.vivy.vivydoctors.manager.impl

import android.content.Context
import android.content.SharedPreferences
import com.vivy.vivydoctors.manager.PreferencesManager
import javax.inject.Inject

class AppPreferencesManager @Inject
constructor(context: Context) : PreferencesManager {

    private val mPrefs: SharedPreferences =
        context.getSharedPreferences("${context.packageName}_data", Context.MODE_PRIVATE)

    companion object {
        private const val PREF_KEY_VIVY_DOCTOR_NAME = "PREF_KEY_VIVY_DOCTOR_NAME"
        private const val PREF_KEY_VIVY_DOCTOR_ID = "PREF_KEY_VIVY_DOCTOR_ID"
    }

    override var vivyDoctorName: String?
        get() = mPrefs.getString(PREF_KEY_VIVY_DOCTOR_NAME,"")
        set(vivyDoctorName) = mPrefs.edit().putString(PREF_KEY_VIVY_DOCTOR_NAME, vivyDoctorName).apply()

    override var vivyDoctorId: String?
        get() = mPrefs.getString(PREF_KEY_VIVY_DOCTOR_ID,"")
        set(vivyDoctorId) = mPrefs.edit().putString(PREF_KEY_VIVY_DOCTOR_ID, vivyDoctorId).apply()

}