package com.vivy.vivydoctors.manager

import com.vivy.vivydoctors.common.dialog.ProgressDialog
import com.vivy.vivydoctors.extension.ViewTaskHandler
import com.vivy.vivydoctors.extension.runOnUI

object ProgressDilogCtrl {

    val elements: MutableList<Any> = mutableListOf()

    fun isEmpty() = elements.isEmpty()

    fun size() = elements.size

    fun push(item: Any) = elements.add(item)

    fun pop(): Any? {
        val item = elements.lastOrNull()
        if (!isEmpty()) {
            elements.removeAt(elements.size - 1)
        }
        return item
    }

    fun peek(): Any? = elements.lastOrNull()

    /**
     * show Dialog
     */
    fun showDialog(isForeground: Boolean, progressLoadingText: String?, dialog: ProgressDialog) {
        if (isForeground) {
            pop()
            push("SHOW")
            ViewTaskHandler.runOnUI {
                dialog.showDialog()
                dialog.setText(progressLoadingText)
            }
        }
    }

    /**
     * hide Dialog
     */
    fun hideDialog(isForeground: Boolean, dialog: ProgressDialog) {
        if (isForeground) {
            pop()
            ViewTaskHandler.runOnUI {
                android.os.Handler().postDelayed({
                    if (isEmpty()) {
                        dialog.closeDialog()
                    }
                }, 500)
            }
        }
    }
}