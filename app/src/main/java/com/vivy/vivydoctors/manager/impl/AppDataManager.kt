package com.vivy.vivydoctors.manager.impl

import android.content.Context
import com.vivy.vivydoctors.manager.DataManager
import com.vivy.vivydoctors.manager.PreferencesManager
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class AppDataManager  @Inject
constructor(
    private val mContext: Context,
    private val mPreferencesHelper: PreferencesManager
): DataManager{
    override var vivyDoctorName: String?
        get() = mPreferencesHelper.vivyDoctorName
        set(doctorName) {
            mPreferencesHelper.vivyDoctorName = doctorName
        }
    override var vivyDoctorId: String?
        get() = mPreferencesHelper.vivyDoctorId
        set(value) {
            mPreferencesHelper.vivyDoctorId = vivyDoctorId
        }
}