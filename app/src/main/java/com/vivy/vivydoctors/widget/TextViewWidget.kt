package com.vivy.vivydoctors.widget

import android.content.Context
import android.graphics.Typeface
import android.text.TextUtils
import android.util.AttributeSet
import android.view.Gravity
import android.view.LayoutInflater
import android.widget.FrameLayout
import androidx.core.content.ContextCompat
import com.vivy.vivydoctors.R
import com.vivy.vivydoctors.enums.TextAlignment
import com.vivy.vivydoctors.enums.TextColor
import com.vivy.vivydoctors.enums.TextSize
import com.vivy.vivydoctors.enums.TextStyle
import com.vivy.vivydoctors.extension.setFontSizeToHeight
import com.vivy.vivydoctors.extension.setFontSizeToWidth
import kotlinx.android.synthetic.main.component_textview.view.*

class TextViewWidget @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr) {

    var textStyle: TextStyle? = null

    init {

        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        inflater.inflate(R.layout.component_textview, this, true)

        attrs?.let {

            val typedArray = context.obtainStyledAttributes(it, R.styleable.TextViewWidget, 0, 0)

            if (typedArray.hasValue(R.styleable.TextViewWidget_textStyle)) {
                textStyle = TextStyle.values()[typedArray.getInt(
                    R.styleable.TextViewWidget_textStyle
                    , 0
                )]
                setTextViewStyle(textStyle)
            }

            if (typedArray.hasValue(R.styleable.TextViewWidget_textColor)) {
                var componentColor = TextColor.values()[typedArray.getInt(
                    R.styleable.TextViewWidget_textColor
                    , 0
                )]
                setTextViewColor(componentColor)
            }

            if (typedArray.hasValue(R.styleable.TextViewWidget_tv_text)) {
                var textViewText = typedArray.getString(
                    R.styleable.TextViewWidget_tv_text
                )

                setTextViewText(textViewText)
            }

            if (typedArray.hasValue(R.styleable.TextViewWidget_textAlignment)) {
                var textAlignment = TextAlignment.values()[typedArray.getInt(
                    R.styleable.TextViewWidget_textAlignment
                    , 0
                )]
                setTextAlignment(textAlignment)
            }

            if (typedArray.hasValue(R.styleable.TextViewWidget_tv_maxLines)) {
                var textViewLines = typedArray.getInt(
                    R.styleable.TextViewWidget_tv_maxLines, 0
                )
                setTextViewNoOfLines(textViewLines)
            }


        }
    }

    /**
     * set font size to device width
     */
    private fun setWidthFontSize(fontWeight: Float): Float {
        return setFontSizeToWidth(fontWeight, context)
    }

    /**
     * set font size to device height
     */
    private fun setHeightFontSize(fontWeight: Float): Float {
        return setFontSizeToHeight(fontWeight, context)
    }

    /**
     * set font typeface
     */
    fun setTypeFace(font: Typeface) {
        componentTextView.typeface = font
    }

    /**
     * set text style
     */
    fun setTextViewStyle(style: TextStyle?) {
        textStyle = style
        when (textStyle) {
            TextStyle.TITLE01H -> {
                componentTextView.textSize = setHeightFontSize(TextSize.TITLE01H.textSize)
            }
            TextStyle.TITLE02H -> {
                componentTextView.textSize = setHeightFontSize(TextSize.TITLE02H.textSize)
            }
            TextStyle.BODY01H -> {
                componentTextView.textSize = setHeightFontSize(TextSize.BODY01H.textSize)
            }
            TextStyle.BODY02H -> {
                componentTextView.textSize = setHeightFontSize(TextSize.BODY02H.textSize)
            }
            TextStyle.BODY03H -> {
                componentTextView.textSize = setHeightFontSize(TextSize.BODY03H.textSize)
            }
            TextStyle.BODY04H -> {
                componentTextView.textSize = setHeightFontSize(TextSize.BODY04H.textSize)
            }
            TextStyle.BTN01H -> {
                componentTextView.textSize = setHeightFontSize(TextSize.BTN01H.textSize)
            }
            TextStyle.BTN02H -> {
                componentTextView.textSize = setHeightFontSize(TextSize.BTN02H.textSize)
            }
            TextStyle.BTN03H -> {
                componentTextView.textSize = setHeightFontSize(TextSize.BTN03H.textSize)
            }
            TextStyle.TITLE01W -> {
                componentTextView.textSize = setWidthFontSize(TextSize.TITLE01W.textSize)
            }
            TextStyle.TITLE02W -> {
                componentTextView.textSize = setWidthFontSize(TextSize.TITLE02W.textSize)
            }
            TextStyle.BODY01W -> {
                componentTextView.textSize = setWidthFontSize(TextSize.BODY01W.textSize)
            }
            TextStyle.BODY02W -> {
                componentTextView.textSize = setWidthFontSize(TextSize.BODY02W.textSize)
            }
            TextStyle.BODY03W -> {
                componentTextView.textSize = setWidthFontSize(TextSize.BODY03W.textSize)
            }
            TextStyle.BODY04W -> {
                componentTextView.textSize = setWidthFontSize(TextSize.BODY04W.textSize)
            }
            TextStyle.BTN01W -> {
                componentTextView.textSize = setWidthFontSize(TextSize.BTN01W.textSize)
            }
            TextStyle.BTN02W -> {
                componentTextView.textSize = setWidthFontSize(TextSize.BTN02W.textSize)
            }
            TextStyle.BTN03W -> {
                componentTextView.textSize = setWidthFontSize(TextSize.BTN03W.textSize)
            }
            else -> {
                componentTextView.textSize = setWidthFontSize(TextSize.BODY01W.textSize)
            }
        }
    }

    /**
     * set text color
     */
    fun setTextViewColor(color: TextColor?) {
        when (color) {
            TextColor.DARK_TV -> {
                componentTextView.setTextColor(
                    ContextCompat.getColor(
                        context,
                        R.color.colorTxtDark
                    )
                )
            }
            TextColor.LIGHT_TV -> {
                componentTextView.setTextColor(
                    ContextCompat.getColor(
                        context,
                        R.color.colorTxtLight
                    )
                )
            }
            TextColor.BLUE_TV -> {
                componentTextView.setTextColor(
                    ContextCompat.getColor(
                        context,
                        R.color.colorTxtBlue
                    )
                )
            }
            TextColor.RED_TV -> {
                componentTextView.setTextColor(ContextCompat.getColor(context, R.color.colorTxtRed))
            }
            TextColor.WHITE_TV -> {
                componentTextView.setTextColor(
                    ContextCompat.getColor(
                        context,
                        R.color.colorTxtWhite
                    )
                )
            }
            else -> {
                componentTextView.setTextColor(
                    ContextCompat.getColor(
                        context,
                        R.color.colorTxtDark
                    )
                )
            }
        }
    }

    /**
     * set text
     */
    fun setTextViewText(text: String?) {
        if (text != null) {
            componentTextView.text = text
        }
    }

    /**
     * set text alignment
     */
    fun setTextAlignment(textAlignment: TextAlignment) {
        when (textAlignment) {
            TextAlignment.LEFT -> {
                componentTextView.gravity = Gravity.LEFT
            }
            TextAlignment.RIGHT -> {
                componentTextView.gravity = Gravity.RIGHT or Gravity.END
            }
            TextAlignment.CENTER -> {
                componentTextView.gravity = Gravity.CENTER
            }
        }
    }

    /**
     * set text lines
     */
    fun setTextViewNoOfLines(noOfLines: Int) {
        componentTextView.minLines = noOfLines
        componentTextView.maxLines = noOfLines
        componentTextView.isSingleLine = false
        componentTextView.ellipsize = TextUtils.TruncateAt.END
        componentTextView.setLines(noOfLines)
    }


}