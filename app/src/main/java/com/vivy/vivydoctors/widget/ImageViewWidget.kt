package com.vivy.vivydoctors.widget

import android.content.Context
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.FrameLayout
import com.squareup.picasso.Picasso
import com.vivy.vivydoctors.R
import com.vivy.vivydoctors.extension.setHorizontalWeight
import com.vivy.vivydoctors.extension.setSizeOnHorizontalWeight
import com.vivy.vivydoctors.extension.setSizeOnVerticalWeight
import com.vivy.vivydoctors.extension.setVerticalWeight
import kotlinx.android.synthetic.main.component_imageview.view.*

class ImageViewWidget @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr) {

    init {
        LayoutInflater.from(context).inflate(R.layout.component_imageview, this, true)

        attrs?.let {

            val typedArray = context.obtainStyledAttributes(it, R.styleable.ImageViewWidget, 0, 0)

            if (typedArray.hasValue(R.styleable.ImageViewWidget_img_drawable)) {
                var imageDrawable = typedArray.getDrawable(
                    R.styleable.ImageViewWidget_img_drawable
                )
                setImage(imageDrawable)
            }
        }
    }

    /**
     * set image size
     * @param height : weight
     * @param width: weight
     */
    fun setImageSize(height: Int, width: Int) {
        imageWrapperView.setVerticalWeight(height)
        imageWrapperView.setHorizontalWeight(width)
    }

    /**
     * set image size on device height
     * @param weight : weight
     */
    fun setImageSizeOnHeight(weight: Int) {
        imageWrapperView.setSizeOnVerticalWeight(weight)
    }

    /**
     * set image size on device width
     * @param weight : weight
     */
    fun setImageSizeOnWidth(weight: Int) {
        imageWrapperView.setSizeOnHorizontalWeight(weight)
    }

    /***
     *set image
     * @param imageDrawable: Image Drawable
     */
    fun setImage(imageDrawable: Drawable?) {
        componentImageView.setImageDrawable(imageDrawable)
    }

    fun setImageUri(uri: String){
        Picasso.get().load(uri).placeholder(R.drawable.ic_avatar).into(componentImageView)
    }

}