package com.vivy.vivydoctors.widget

import android.content.Context
import android.content.res.ColorStateList
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.LinearLayout
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.core.view.isVisible
import com.google.android.material.button.MaterialButton
import com.vivy.vivydoctors.R
import com.vivy.vivydoctors.enums.ButtonSize
import com.vivy.vivydoctors.enums.ButtonType
import com.vivy.vivydoctors.enums.TextSize
import com.vivy.vivydoctors.extension.setFontSizeToWidth
import com.vivy.vivydoctors.util.ScreenUtil
import kotlinx.android.synthetic.main.component_button.view.*

class ButtonWidget @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr) {

    init {
        LayoutInflater.from(context).inflate(R.layout.component_button, this, true)

        attrs?.let {

            val typedArray = context.obtainStyledAttributes(it, R.styleable.ButtonWidget, 0, 0)

            if (typedArray.hasValue(R.styleable.ButtonWidget_buttonType)) {
                var componentType = ButtonType.values()[typedArray.getInt(
                    R.styleable.ButtonWidget_buttonType
                    , 0
                )]
                setButtonType(componentType)
            }

            if (typedArray.hasValue(R.styleable.ButtonWidget_buttonSize)) {
                var componentSize = ButtonSize.values()[typedArray.getInt(
                    R.styleable.ButtonWidget_buttonSize
                    , 0
                )]
                setButtonSize(componentSize)
            }

            if (typedArray.hasValue(R.styleable.ButtonWidget_btn_text)) {
                var btnText = typedArray.getString(
                    R.styleable.ButtonWidget_btn_text
                )
                setButtonText(btnText)
            }
        }
    }

    /**
     * set button type
     * @param type : ButtonType
     */
    fun setButtonType(type: ButtonType) {
        when (type) {
            ButtonType.FILLED_DARK_BLUE -> {
                buttonComponent.visibility = View.VISIBLE
                buttonOutlinedComponent.visibility = View.GONE
                buttonTextComponent.visibility = View.GONE
                buttonComponent.setBackgroundColor(
                    ContextCompat.getColor(
                        context,
                        R.color.colorBtnDarkBlue
                    )
                )
                buttonComponent.setTextColor(ContextCompat.getColor(context, R.color.colorBtnWhite))
                buttonComponent.rippleColor =
                    ColorStateList.valueOf(
                        ContextCompat.getColor(
                            context,
                            R.color.colorBtnDarkBlue
                        )
                    )
            }
            ButtonType.FILLED_LIGHT_BLUE -> {
                buttonComponent.visibility = View.VISIBLE
                buttonOutlinedComponent.visibility = View.GONE
                buttonTextComponent.visibility = View.GONE
                buttonComponent.setBackgroundColor(
                    ContextCompat.getColor(
                        context,
                        R.color.colorBtnLightBlue
                    )
                )
                buttonComponent.setTextColor(ContextCompat.getColor(context, R.color.colorBtnWhite))
                buttonComponent.rippleColor =
                    ColorStateList.valueOf(
                        ContextCompat.getColor(
                            context,
                            R.color.colorBtnLightBlue
                        )
                    )
            }
            ButtonType.FILLED_RED -> {
                buttonComponent.visibility = View.VISIBLE
                buttonOutlinedComponent.visibility = View.GONE
                buttonTextComponent.visibility = View.GONE
                buttonComponent.setBackgroundColor(
                    ContextCompat.getColor(
                        context,
                        R.color.colorBtnRed
                    )
                )
                buttonComponent.setTextColor(ContextCompat.getColor(context, R.color.colorBtnWhite))
                buttonComponent.rippleColor =
                    ColorStateList.valueOf(ContextCompat.getColor(context, R.color.colorBtnRed))
            }
            ButtonType.FILLED_GREY -> {
                buttonComponent.visibility = View.VISIBLE
                buttonOutlinedComponent.visibility = View.GONE
                buttonTextComponent.visibility = View.GONE
                buttonComponent.setBackgroundColor(
                    ContextCompat.getColor(
                        context,
                        R.color.colorBtnWhite
                    )
                )
                buttonComponent.setTextColor(ContextCompat.getColor(context, R.color.colorBtnLight))
                buttonComponent.rippleColor =
                    ColorStateList.valueOf(ContextCompat.getColor(context, R.color.colorBtnLight))
            }
            ButtonType.FILLED_BLACK -> {
                buttonComponent.visibility = View.VISIBLE
                buttonOutlinedComponent.visibility = View.GONE
                buttonTextComponent.visibility = View.GONE
                buttonComponent.setBackgroundColor(
                    ContextCompat.getColor(
                        context,
                        R.color.colorBtnDark
                    )
                )
                buttonComponent.setTextColor(ContextCompat.getColor(context, R.color.colorBtnWhite))
                buttonComponent.rippleColor =
                    ColorStateList.valueOf(ContextCompat.getColor(context, R.color.colorBtnDark))
            }
            ButtonType.OUTLINED_DARK_BLUE -> {
                buttonOutlinedComponent.visibility = View.VISIBLE
                buttonComponent.visibility = View.GONE
                buttonTextComponent.visibility = View.GONE
                buttonOutlinedComponent.setTextColor(
                    ContextCompat.getColor(
                        context,
                        R.color.colorBtnDarkBlue
                    )
                )
                buttonOutlinedComponent.strokeColor =
                    ColorStateList.valueOf(
                        ContextCompat.getColor(
                            context,
                            R.color.colorBtnDarkBlue
                        )
                    )
                buttonComponent.rippleColor =
                    ColorStateList.valueOf(
                        ContextCompat.getColor(
                            context,
                            R.color.colorBtnDarkBlue
                        )
                    )
            }
            ButtonType.OUTLINED_LIGHT_BLUE -> {
                buttonOutlinedComponent.visibility = View.VISIBLE
                buttonComponent.visibility = View.GONE
                buttonTextComponent.visibility = View.GONE
                buttonOutlinedComponent.setTextColor(
                    ContextCompat.getColor(
                        context,
                        R.color.colorBtnLightBlue
                    )
                )
                buttonOutlinedComponent.strokeColor =
                    ColorStateList.valueOf(
                        ContextCompat.getColor(
                            context,
                            R.color.colorBtnLightBlue
                        )
                    )
                buttonComponent.rippleColor =
                    ColorStateList.valueOf(
                        ContextCompat.getColor(
                            context,
                            R.color.colorBtnLightBlue
                        )
                    )
            }
            ButtonType.OUTLINED_RED -> {
                buttonOutlinedComponent.visibility = View.VISIBLE
                buttonComponent.visibility = View.GONE
                buttonTextComponent.visibility = View.GONE
                buttonOutlinedComponent.setTextColor(
                    ContextCompat.getColor(
                        context,
                        R.color.colorBtnRed
                    )
                )
                buttonOutlinedComponent.strokeColor =
                    ColorStateList.valueOf(ContextCompat.getColor(context, R.color.colorBtnRed))
                buttonComponent.rippleColor =
                    ColorStateList.valueOf(ContextCompat.getColor(context, R.color.colorBtnRed))
            }
            ButtonType.OUTLINED_GREY -> {
                buttonOutlinedComponent.visibility = View.VISIBLE
                buttonComponent.visibility = View.GONE
                buttonTextComponent.visibility = View.GONE
                buttonOutlinedComponent.setTextColor(
                    ContextCompat.getColor(
                        context,
                        R.color.colorBtnLight
                    )
                )
                buttonOutlinedComponent.strokeColor =
                    ColorStateList.valueOf(ContextCompat.getColor(context, R.color.colorBtnLight))
                buttonComponent.rippleColor =
                    ColorStateList.valueOf(ContextCompat.getColor(context, R.color.colorBtnLight))
            }
            ButtonType.OUTLINED_BLACK -> {
                buttonOutlinedComponent.visibility = View.VISIBLE
                buttonComponent.visibility = View.GONE
                buttonTextComponent.visibility = View.GONE
                buttonOutlinedComponent.setTextColor(
                    ContextCompat.getColor(
                        context,
                        R.color.colorBtnDark
                    )
                )
                buttonOutlinedComponent.strokeColor =
                    ColorStateList.valueOf(ContextCompat.getColor(context, R.color.colorBtnDark))
                buttonComponent.rippleColor =
                    ColorStateList.valueOf(ContextCompat.getColor(context, R.color.colorBtnDark))
            }
            ButtonType.TEXT_DARK_BLUE -> {
                buttonTextComponent.visibility = View.VISIBLE
                buttonComponent.visibility = View.GONE
                buttonOutlinedComponent.visibility = View.GONE
                buttonTextComponent.setTextColor(
                    ContextCompat.getColor(
                        context,
                        R.color.colorBtnDarkBlue
                    )
                )
                buttonComponent.rippleColor =
                    ColorStateList.valueOf(
                        ContextCompat.getColor(
                            context,
                            R.color.colorBtnDarkBlue
                        )
                    )
            }
            ButtonType.TEXT_LIGHT_BLUE -> {
                buttonTextComponent.visibility = View.VISIBLE
                buttonComponent.visibility = View.GONE
                buttonOutlinedComponent.visibility = View.GONE
                buttonTextComponent.setTextColor(
                    ContextCompat.getColor(
                        context,
                        R.color.colorBtnLightBlue
                    )
                )
                buttonComponent.rippleColor =
                    ColorStateList.valueOf(
                        ContextCompat.getColor(
                            context,
                            R.color.colorBtnLightBlue
                        )
                    )
            }
            ButtonType.TEXT_RED -> {
                buttonTextComponent.visibility = View.VISIBLE
                buttonComponent.visibility = View.GONE
                buttonOutlinedComponent.visibility = View.GONE
                buttonTextComponent.setTextColor(
                    ContextCompat.getColor(
                        context,
                        R.color.colorBtnRed
                    )
                )
                buttonComponent.rippleColor =
                    ColorStateList.valueOf(ContextCompat.getColor(context, R.color.colorBtnRed))
            }
            ButtonType.TEXT_GREY -> {
                buttonTextComponent.visibility = View.VISIBLE
                buttonComponent.visibility = View.GONE
                buttonOutlinedComponent.visibility = View.GONE
                buttonTextComponent.setTextColor(
                    ContextCompat.getColor(
                        context,
                        R.color.colorBtnLight
                    )
                )
                buttonComponent.rippleColor =
                    ColorStateList.valueOf(ContextCompat.getColor(context, R.color.colorBtnLight))
            }
            ButtonType.TEXT_BLACK -> {
                buttonTextComponent.visibility = View.VISIBLE
                buttonComponent.visibility = View.GONE
                buttonOutlinedComponent.visibility = View.GONE
                buttonTextComponent.setTextColor(
                    ContextCompat.getColor(
                        context,
                        R.color.colorBtnDark
                    )
                )
                buttonComponent.rippleColor =
                    ColorStateList.valueOf(ContextCompat.getColor(context, R.color.colorBtnDark))
            }
        }
    }

    /**
     * set button size
     * size : ButtonSize
     */
    fun setButtonSize(size: ButtonSize?) {
        when (size) {
            ButtonSize.SMALL_BTN -> {
                getButton().textSize = setButtonFontSize(TextSize.BTN01W.textSize)
                getButton().typeface = ResourcesCompat.getFont(context, R.font.sfuitext_medium)
            }
            ButtonSize.MEDIUM_BTN -> {
                getButton().textSize = setButtonFontSize(TextSize.BTN02W.textSize)
                getButton().typeface = ResourcesCompat.getFont(context, R.font.sfuitext_medium)
            }
            ButtonSize.LARGE_BTN -> {
                getButton().textSize = setButtonFontSize(TextSize.BTN03W.textSize)
                getButton().typeface = ResourcesCompat.getFont(context, R.font.sfuitext_semibold)
            }
        }
    }

    /**
     * set font size to device width
     * @param weight : Float
     */
    fun setButtonFontSize(weight: Float): Float {
        return setFontSizeToWidth(weight, context)
    }

    /**
     * set button size on device width
     * @param width : Int
     * @param height: Int
     */
    fun setButtonSizeOnWidth(width: Int, height: Int) {
        getButton().layoutParams =
            LinearLayout.LayoutParams(
                (ScreenUtil.width / 100) * width,
                (ScreenUtil.width / 100) * height
            )
    }

    /**
     * set button size on device height
     * @param width : Int
     * @param height: Int
     */
    fun setButtonSizeOnHeight(width: Int, height: Int) {
        getButton().layoutParams =
            LinearLayout.LayoutParams(
                (ScreenUtil.height / 100) * width,
                (ScreenUtil.height / 100) * height
            )
    }

    /**
     * set button height on device height
     * @param height : Int
     */
    fun setButtonHeightOnHeight(height: Int) {
        getButton().layoutParams =
            LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                (ScreenUtil.height / 100) * height
            )
    }

    /**
     * set button height on device width
     * @param height : Int
     */
    fun setButtonHeightOnWidth(height: Int) {
        getButton().layoutParams =
            LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                (ScreenUtil.width / 100) * height
            )
    }

    /**
     * set button width on device height
     * @param width : Int
     */
    fun setButtonWidthOnHeight(width: Int) {
        getButton().layoutParams =
            LinearLayout.LayoutParams(
                (ScreenUtil.height / 100) * width,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )
    }

    /**
     * set button width on device width
     * @param width : Int
     */
    fun setButtonWidthOnWidth(width: Int) {
        getButton().layoutParams =
            LinearLayout.LayoutParams(
                (ScreenUtil.width / 100) * width,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )
    }

    /**
     * get button
     */
    fun getButton(): MaterialButton {
        return when {
            buttonComponent.isVisible -> buttonComponent
            buttonOutlinedComponent.isVisible -> buttonOutlinedComponent
            buttonTextComponent.isVisible -> buttonTextComponent
            else -> buttonComponent
        }
    }

    /**
     * set button text
     */
    fun setButtonText(text: String?) {
        getButton().text = text
    }

    /**
     * set onclick listener
     */
    fun setListener(onClick: () -> Unit) {
        getButton().setOnClickListener {
            onClick()
        }
    }

    /**
     * enable button click
     */
    fun enableButton(isEnabled: Boolean) {
        getButton().isEnabled = isEnabled
    }
}