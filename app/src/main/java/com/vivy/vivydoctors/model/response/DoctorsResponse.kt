package com.vivy.vivydoctors.model.response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

data class DoctorsResponse(
    @SerializedName("doctors") val doctors : ArrayList<DoctorDetails>,
    @SerializedName("lastKey") val lastKey : String
)

@Parcelize
data class DoctorDetails (
    @SerializedName("id") val id : String,
    @SerializedName("name") val name : String,
    @SerializedName("photoId") val photoId : String,
    @SerializedName("rating") val rating : Float,
    @SerializedName("address") val address : String,
    @SerializedName("lat") val lat : Double,
    @SerializedName("lng") val lng : Double,
    @SerializedName("highlighted") val highlighted : Boolean,
    @SerializedName("reviewCount") val reviewCount : Int,
    @SerializedName("specialityIds") val specialityIds : ArrayList<Int>,
    @SerializedName("source") val source : String,
    @SerializedName("phoneNumber") val phoneNumber : String,
    @SerializedName("email") val email : String,
    @SerializedName("website") val website : String,
    @SerializedName("openingHours") val openingHours : ArrayList<String>,
    @SerializedName("integration") val integration : String,
    @SerializedName("translation") val translation : String,
    var recentSelected : Int


): Parcelable