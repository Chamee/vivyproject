package com.vivy.vivydoctors.base

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import com.vivy.vivydoctors.R
import com.vivy.vivydoctors.common.dialog.ErrorDialog
import com.vivy.vivydoctors.extension.ViewTaskHandler
import com.vivy.vivydoctors.extension.runOnUI
import dagger.android.support.AndroidSupportInjection

abstract class BaseFragment<T : ViewDataBinding, V: BaseViewModel> : Fragment() {

    private var baseActivity: BaseActivity<*, *>? = null

    private var mRootView: View? = null
    private lateinit var viewDataBinding: T
    private var mViewModel: V? = null

    /**
     * Override for set binding variable
     *
     * @return variable id
     */
    abstract val bindingVariable: Int

    /**
     * @return layout resource id
     */
    @get:LayoutRes
    abstract val layoutId: Int

    /**
     * Override for set view model
     *
     * @return view model instance
     */
    abstract val viewModel: V

    override fun onAttach(context: Context) {

        performDependencyInjection()
        super.onAttach(context)
        if (context is BaseActivity<*, *>) {
            val activity = context as BaseActivity<*, *>?
            this.baseActivity = activity
            activity!!.onFragmentAttached()
        }

    }

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        mViewModel = viewModel
        setHasOptionsMenu(false)
    }

    override fun onCreateView(

        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewDataBinding = DataBindingUtil.inflate(inflater, layoutId, container, false)
        mRootView = viewDataBinding.root
        return mRootView

    }

    override fun onDetach() {

        baseActivity = null
        super.onDetach()

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        super.onViewCreated(view, savedInstanceState)
        viewDataBinding.setVariable(bindingVariable, mViewModel)
        viewDataBinding.lifecycleOwner = this
        viewDataBinding.executePendingBindings()

    }

    /**
     * Return Data Binding
     *
     * @return : viewDataBinding
     */
    fun getViewDataBinding(): T {

        return viewDataBinding

    }

    private fun performDependencyInjection() {

        AndroidSupportInjection.inject(this)

    }

    /**
     * Show error messages
     *
     * @param message   Error message to be shown
     */
    fun showError(message: String? = null) {

        ViewTaskHandler.runOnUI {
            message?.let {
                showErrorMessage(this.resources.getString(R.string.error_title), message)
            } ?: run {
                showErrorMessage(
                    resources.getString(R.string.error_title),
                    resources.getString(R.string.error_message)
                )
            }
        }

    }

    /**
     * Setup error message dialog
     *
     * @param title     error title
     * @param message   error message
     */
    private fun showErrorMessage(title: String?, message: String?) {

        var errorDialogHelper = context?.let { ErrorDialog(it) }
        errorDialogHelper?.setErrorText(title, message)
        errorDialogHelper?.showDialog()

    }
    /**
     * Add Fragment
     * @param fragment : Fragment to Add
     * @param tag : TAG
     * @param bundle : Extra Data
     */
    fun addFragment(fragment: Fragment, tag: String, bundle: Bundle? = null) {

        baseActivity?.addFragment(fragment, tag, bundle)
    }


    interface Callback {

        fun onFragmentAttached()

        fun onFragmentDetached(tag: String)
    }
}