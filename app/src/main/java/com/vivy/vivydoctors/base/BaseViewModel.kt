package com.vivy.vivydoctors.base

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.vivy.vivydoctors.manager.DataManager
import io.reactivex.disposables.CompositeDisposable

abstract class BaseViewModel(private val dataManager: DataManager, private var context: Context) :
    ViewModel() {

    private var compositeDisposable: CompositeDisposable = CompositeDisposable()

    private var progressLoadingText: String? = null

    var onError: MutableLiveData<Throwable> = MutableLiveData()

    override fun onCleared() {

        compositeDisposable.dispose()
        super.onCleared()

    }

    fun getCompositeDisposable(): CompositeDisposable {

        return compositeDisposable

    }

    fun getDataManager(): DataManager {

        return dataManager

    }

    fun getProgressLoadingText(): String? {

        return progressLoadingText

    }

    fun setProgressLoadingText(text: String) {

        progressLoadingText = text

    }

    protected fun onError(err: Throwable) {
        onError.value = err
    }


}