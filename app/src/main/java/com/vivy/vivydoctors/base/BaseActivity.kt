package com.vivy.vivydoctors.base

import android.os.Bundle
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import com.vivy.vivydoctors.R
import dagger.android.AndroidInjection
import io.reactivex.annotations.Nullable

abstract class BaseActivity<T : ViewDataBinding, V : BaseViewModel> : AppCompatActivity(),
    BaseFragment.Callback {

    private lateinit var viewDataBinding: T
    private var mViewModel: V? = null

    /**
     * Override to set binding variable
     *
     * @return variable id
     */
    abstract val bindingVariable: Int

    /**
     * @return layout resource id
     */
    @get:LayoutRes
    abstract val layoutId: Int

    /**
     * Override to set view model
     *
     * @return view model instance
     */
    abstract val viewModel: V

    /* function callback when fragment attached */
    override fun onFragmentAttached() {

    }

    /*function callback when fragment detached*/
    override fun onFragmentDetached(tag: String) {

    }

    override fun onCreate(@Nullable savedInstanceState: Bundle?) {

        performDependencyInjection()
        super.onCreate(savedInstanceState)
        performDataBinding()
        initActivity()
//        ApiRouter.initializeContext(this, getProgressListener())

    }

    /**
     * init App Data
     */
    private fun initActivity() {


    }

//    /**
//     * Get Progress Listener
//     */
//    private fun getProgressListener(): ServiceListener {
//
//        return object : ServiceListener {
//            override fun onServiceStart(isForeground: Boolean) {
//                if (isForeground) {
//                    progressDialog?.let {
//                        ProgressDialogCtrl.showDialog(
//                            isForeground,
//                            viewModel.getProgressLoadingText(), it
//                        )
//                    }
//                }
//            }
//
//            override fun onServiceStop(isForeground: Boolean) {
//                if (isForeground) {
//                    progressDialog?.let {
//                        ProgressDialogCtrl.hideDialog(isForeground, it)
//                    }
//                }
//            }
//        }
//
//    }

    /**
     * add Fragment to Container
     */
    fun addFragment(
        fragment: Fragment,
        tag: String,
        bundle: Bundle? = null,
        isAddToBackStack: Boolean = true
    ) {

        bundle?.let {
            fragment.arguments = bundle
        }
        val fragmentManager = this.supportFragmentManager
        val ft = fragmentManager?.beginTransaction()
        if (isAddToBackStack) {
            ft?.add(R.id.container, fragment, tag)
            ft?.addToBackStack(tag)
        } else {
            ft?.replace(R.id.container, fragment, tag)
        }
        ft?.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
        ft?.commit()
    }

    fun getViewDataBinding(): T {

        return viewDataBinding

    }

    private fun performDependencyInjection() {

        AndroidInjection.inject(this)

    }

    private fun performDataBinding() {

        viewDataBinding = DataBindingUtil.setContentView(this, layoutId)
        this.mViewModel = if (mViewModel == null) viewModel else mViewModel
        viewDataBinding.setVariable(bindingVariable, mViewModel)
        viewDataBinding.executePendingBindings()

    }
}