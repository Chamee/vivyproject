package com.base.app.config

class AppConfig{

    companion object {

        var VERSION_NAME = "1.0.0"
        var TYPE = ""
        var IS_DEV = false
        var BASE_URL = "https://vivy.com/interviews/challenges/android/"
        var IS_SSL_ENABLE = false
        var RECENT_SEARCH_COUNT = 3
    }

}