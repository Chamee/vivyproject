package com.vivy.vivydoctors.common.dialog

import android.app.Dialog
import android.content.Context
import android.util.Log
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import com.vivy.vivydoctors.R
import kotlinx.android.synthetic.main.dialog_progress_layout.*

class ProgressDialog(context: Context) :
    Dialog(context, android.R.style.Theme_Translucent_NoTitleBar) {

    private lateinit var animationFadeIn: Animation

    init {

        this.setContentView(R.layout.dialog_progress_layout)
        this.setCancelable(true)

        initialize()

        getData()

        setData()

        setListeners()

    }

    /**
     * Initialize
     */
    private fun initialize() {

    }

    /**
     * Get data
     */
    private fun getData() {

    }

    /**
     * Set data
     */
    private fun setData() {

        animationFadeIn = AnimationUtils.loadAnimation(context, R.anim.fade_back)

    }

    /**
     * Set listeners
     */
    private fun setListeners() {

    }

    /**
     * Set progress text
     *
     * @param text  Progress text
     */
    fun setText(text: String?) {

        progressTxt.text = text

    }

    /**
     * Show dialog
     */
    fun showDialog() {

        if (!isShowing) {
            try {
                this.show()
            } catch (ex: Exception) {
                Log.e("Exception Dialog", "")
            }
        }

    }

    /**
     * Close dialog
     */
    fun closeDialog() {

        try {
            if (isShowing) {
                this.dismiss()
            }
        } catch (ex: IllegalArgumentException) {

        }

    }

}