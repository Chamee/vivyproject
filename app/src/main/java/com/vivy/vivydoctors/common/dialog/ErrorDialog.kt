package com.vivy.vivydoctors.common.dialog

import android.app.Dialog
import android.content.Context
import com.vivy.vivydoctors.R
import kotlinx.android.synthetic.main.dialog_error.*

class ErrorDialog(context: Context) :
    Dialog(context!!, android.R.style.Theme_Translucent_NoTitleBar){

    init {

        this.setContentView(R.layout.dialog_error)
        this.setCancelable(false)

        defineLayout()

        getData()

        setData()

        setListeners()

    }

    /**
     * Define layout
     */
    private fun defineLayout() {

    }

    /**
     * Set data
     */
    private fun setData() {

        errorTitleTv.setTextViewText(context.resources?.getString(R.string.error_title))
        errorMessageTv.setTextViewText(context.resources?.getString(R.string.error_message))
        okBtn.setButtonText(context.resources?.getString(R.string.error_button))

    }

    /**
     * Get data
     */
    private fun getData() {

    }

    /**
     * Set listeners
     */
    private fun setListeners() {

        okBtn.setListener {
            closeDialog()
        }

    }

    /**
     * Show dialog
     */
    fun showDialog() {

        try {
            if (!this.isShowing) {
                show()
            }
        } catch (ex: java.lang.Exception) {
        }

    }

    /**
     * Close dialog
     */
    fun closeDialog() {

        try {
            if (this.isShowing) {
                this.dismiss()
            }
        } catch (e: Exception) {
        }

    }

    /**
     * Set error data
     */
    fun setErrorText(heading: String?, body: String?) {

        errorTitleTv.setTextViewText(heading)
        errorMessageTv.setTextViewText(body)

    }


    /**
     * Set cancel onclick listener
     */
    fun setListener(onClick: () -> Unit) {

        okBtn.setListener(onClick)

    }
}