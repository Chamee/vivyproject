package com.vivy.vivydoctors.enums

enum class TextColor {
    DARK_TV, LIGHT_TV, BLUE_TV, RED_TV, WHITE_TV;
}