package com.vivy.vivydoctors.enums

enum class ButtonSize {
    SMALL_BTN, MEDIUM_BTN, LARGE_BTN;
}