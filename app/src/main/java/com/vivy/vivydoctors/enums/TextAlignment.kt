package com.vivy.vivydoctors.enums

enum class TextAlignment {
    LEFT, RIGHT, CENTER;
}