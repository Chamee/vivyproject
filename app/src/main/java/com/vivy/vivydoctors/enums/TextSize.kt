package com.vivy.vivydoctors.enums

enum class TextSize(var textSize: Float) {
    TITLE01H(3.0f), TITLE02H(2.4f), BODY01H(2.1f), BODY02H(2.4f), BODY03H(3.5f), BODY04H(1.6f), BTN01H(
        1.9f
    ),
    BTN02H(2.3f), BTN03H(3.1f),
    TITLE01W(4.8f), TITLE02W(3.7f), BODY01W(3.4f), BODY02W(3.7f), BODY03W(5.0f), BODY04W(2.9f), BTN01W(
        3.0f
    ),
    BTN02W(3.5f), BTN03W(4.5f);
}